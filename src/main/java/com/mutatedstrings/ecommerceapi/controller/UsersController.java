package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.user.GetDetailedUserResponse;
import com.mutatedstrings.ecommerceapi.dto.user.GetUserResponse;
import com.mutatedstrings.ecommerceapi.dto.user.PostUserRequest;
import com.mutatedstrings.ecommerceapi.model.User;
import com.mutatedstrings.ecommerceapi.service.UserService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.USERS)
public class UsersController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<GetUserResponse> getAll() {
        return ObjectMapperUtil.mapAll(userService.getAll(), GetUserResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetDetailedUserResponse> get(@PathVariable String id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(userService.get(id), GetDetailedUserResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetUserResponse> create(@RequestBody PostUserRequest postUserRequest) {
        User user = userService.create(ObjectMapperUtil.map(postUserRequest, User.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(user, GetUserResponse.class));
    }

    @PostMapping("/login")
    public ResponseEntity<GetUserResponse> login(@RequestBody PostUserRequest postUserRequest) {
        User user = userService.getUserByEmailAndPass(postUserRequest.getEmail(), postUserRequest.getPassword());
        return ResponseEntity.ok().body(ObjectMapperUtil.map(user, GetUserResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetUserResponse> update(@RequestBody PostUserRequest postUserRequest, @PathVariable String id) {
        postUserRequest.setUserId(id);
        User user = userService.update(ObjectMapperUtil.map(postUserRequest, User.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(user, GetUserResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
