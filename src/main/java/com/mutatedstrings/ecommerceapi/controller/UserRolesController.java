package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.userRole.GetUserRoleResponse;
import com.mutatedstrings.ecommerceapi.dto.userRole.PostUserRoleRequest;
import com.mutatedstrings.ecommerceapi.model.UserRole;
import com.mutatedstrings.ecommerceapi.service.UserRoleService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.function.Function;

@RestController
@RequestMapping(ApiName.USER_ROLES)
public class UserRolesController {
    @Autowired
    private UserRoleService userRoleService;

    @GetMapping
    public Page<GetUserRoleResponse> getAll(Pageable pageable) {
        return ObjectMapperUtil.map(userRoleService.getAll(pageable), GetUserRoleResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetUserRoleResponse> get(@PathVariable String id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(userRoleService.get(id), GetUserRoleResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetUserRoleResponse> create(@RequestBody PostUserRoleRequest postUserRoleRequest) {
        UserRole userRole = userRoleService.create(ObjectMapperUtil.map(postUserRoleRequest, UserRole.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(userRole, GetUserRoleResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetUserRoleResponse> update(@RequestBody PostUserRoleRequest postUserRoleRequest, @PathVariable String id) {
        UserRole userRole = userRoleService.update(ObjectMapperUtil.map(postUserRoleRequest, UserRole.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(userRole, GetUserRoleResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        userRoleService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
