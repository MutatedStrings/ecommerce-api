package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.user.creditCard.GetCreditCardResponse;
import com.mutatedstrings.ecommerceapi.model.CreditCard;
import com.mutatedstrings.ecommerceapi.service.CreditCardService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.CREDIT_CARDS)
public class CreditCardsController {
    @Autowired
    private CreditCardService creditCardService;

    @GetMapping
    public List<GetCreditCardResponse> getAll(@PathVariable String userId) {
        return ObjectMapperUtil.mapAll(creditCardService.getAllByUserId(userId), GetCreditCardResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetCreditCardResponse> get(@PathVariable String userId, @PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(creditCardService.get(userId, id), GetCreditCardResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetCreditCardResponse> create(@RequestBody GetCreditCardResponse getCreditCardResponse,
                                                        @PathVariable String userId) {
        getCreditCardResponse.setUserId(userId);
        CreditCard creditCard = creditCardService.create(ObjectMapperUtil.map(getCreditCardResponse, CreditCard.class), userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(creditCard, GetCreditCardResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetCreditCardResponse> update(@RequestBody GetCreditCardResponse getCreditCardResponse,
                                                        @PathVariable String userId, @PathVariable Integer id) {
        getCreditCardResponse.setCardId(id);
        getCreditCardResponse.setUserId(userId);
        CreditCard creditCard = creditCardService.update(ObjectMapperUtil.map(getCreditCardResponse, CreditCard.class), userId);
        return ResponseEntity.ok(ObjectMapperUtil.map(creditCard, GetCreditCardResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String userId, @PathVariable Integer id) {
        creditCardService.delete(userId, id);
        return ResponseEntity.noContent().build();
    }
}
