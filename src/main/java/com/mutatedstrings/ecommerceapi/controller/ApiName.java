package com.mutatedstrings.ecommerceapi.controller;

public class ApiName {
    // user api links
    public static final String USERS = "/users";
    public static final String USER = USERS + "/{userId}";

    // billing address api links
    public static final String BILLING_ADDRESSES = USER + "/billingaddresses";

    // cart item api links
    public static final String CART_ITEMS = USER + "/cartitems";

    // credit card api links
    public static final String CREDIT_CARDS = USER + "/creditcards";

    // order api links
    public static final String ORDERS = "/orders";

    // order item api links
    public static final String ORDER_ITEMS = "/orderitems";

    // category api links
    public static final String CATEGORIES = "/categories";

    // product api links
    public static final String PRODUCTS = "/products";
    public static final String PRODUCT = PRODUCTS + "/{productId}";

    // product variation api links
    public static final String PRODUCT_VARIATIONS = PRODUCT + "/productvariations";

    // user role api links
    public static final String USER_ROLES = "/userroles";
}
