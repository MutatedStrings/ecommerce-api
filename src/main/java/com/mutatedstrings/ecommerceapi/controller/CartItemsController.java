package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.user.cartItem.GetCartItemResponse;
import com.mutatedstrings.ecommerceapi.dto.user.cartItem.PostCartItemRequest;
import com.mutatedstrings.ecommerceapi.model.CartItem;
import com.mutatedstrings.ecommerceapi.service.CartItemService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.CART_ITEMS)
public class CartItemsController {
    @Autowired
    private CartItemService cartItemService;

    @GetMapping
    public List<GetCartItemResponse> getAll(@PathVariable String userId) {
        return ObjectMapperUtil.mapAll(cartItemService.getAllByUserId(userId), GetCartItemResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetCartItemResponse> get(@PathVariable String userId, @PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(cartItemService.get(userId, id), GetCartItemResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetCartItemResponse> create(@RequestBody PostCartItemRequest postCartItemRequest, @PathVariable String userId) {
        postCartItemRequest.setUserId(userId);
        CartItem cartItem = cartItemService.create(ObjectMapperUtil.map(postCartItemRequest, CartItem.class), userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(cartItem, GetCartItemResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetCartItemResponse> update(@RequestBody PostCartItemRequest postCartItemRequest,
                                                      @PathVariable String userId, @PathVariable Integer id) {
        postCartItemRequest.setCartItemId(id);
        postCartItemRequest.setUserId(userId);
        CartItem cartItem = cartItemService.update(ObjectMapperUtil.map(postCartItemRequest, CartItem.class), userId);
        return ResponseEntity.ok(ObjectMapperUtil.map(cartItem, GetCartItemResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String userId, @PathVariable Integer id) {
        cartItemService.delete(userId, id);
        return ResponseEntity.noContent().build();
    }

    /**
     * Deletes all the cart items in a particular user's cart
     * @param userId
     * @return  Status code 204
     */
    @DeleteMapping
    public ResponseEntity deleteAllCartItems(@PathVariable String userId) {
        cartItemService.deleteAllByUser(userId);
        return ResponseEntity.noContent().build();
    }
}
