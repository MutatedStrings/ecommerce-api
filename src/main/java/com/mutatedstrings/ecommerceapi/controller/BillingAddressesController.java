package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.user.billingAddress.GetBillingAddressResponse;
import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.BillingAddress;
import com.mutatedstrings.ecommerceapi.service.BillingAddressService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.BILLING_ADDRESSES)
public class BillingAddressesController {
    @Autowired
    private BillingAddressService billingAddressService;

    /**
     * Get all billing addresses for a particular user
     * @param userId
     * @return  List of GetBillingAddressResponse Dto objects. Status Code 200
     */
    @GetMapping
    public List<GetBillingAddressResponse> getAll(@PathVariable String userId) {
        return ObjectMapperUtil.mapAll(billingAddressService.getAllByUserId(userId), GetBillingAddressResponse.class);
    }

    /**
     * Get details of a billing address
     * @param userId    User id
     * @param id        Billing address id
     * @return          GetBillingAddressResponse Dto object. Status Code 200 if successful
     */
    @GetMapping("/{id}")
    public ResponseEntity<GetBillingAddressResponse> get(@PathVariable String userId, @PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(billingAddressService.get(userId, id), GetBillingAddressResponse.class));
    }

    /**
     * Create a new billing address
     * @param getBillingAddressResponse GetBillingAddressResponse Dto Object
     * @param userId                    User id
     * @return                          Created GetBillingAddressResponse Dto object. Status code 201 if successful
     */
    @PostMapping
    public ResponseEntity<GetBillingAddressResponse> create(@RequestBody GetBillingAddressResponse getBillingAddressResponse, @PathVariable String userId) {
        getBillingAddressResponse.setUserId(userId);
        BillingAddress billingAddress = billingAddressService.create(ObjectMapperUtil.map(getBillingAddressResponse, BillingAddress.class), userId);
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(billingAddress, GetBillingAddressResponse.class));
    }

    /**
     * Update billing address details
     * @param getBillingAddressResponse GetBillingAddressResponse Dto Object
     * @param userId                    User id
     * @param id                        Billing address id
     * @return                          Updated GetBillingAddressResponse Dto object. Status code 200
     */
    @PutMapping("/{id}")
    public ResponseEntity<GetBillingAddressResponse> update(@RequestBody GetBillingAddressResponse getBillingAddressResponse,
                                                            @PathVariable String userId, @PathVariable Integer id) {
        getBillingAddressResponse.setAddressId(id);
        getBillingAddressResponse.setUserId(userId);
        BillingAddress billingAddress = billingAddressService.update(ObjectMapperUtil.map(getBillingAddressResponse, BillingAddress.class), userId);
        return ResponseEntity.ok(ObjectMapperUtil.map(billingAddress, GetBillingAddressResponse.class));
    }

    /**
     * Deletes the billing address
     * @param userId    User id
     * @param id        Billing address id
     * @return          Status code 204 when successfully deleted
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String userId, @PathVariable Integer id) {
        billingAddressService.delete(userId, id);
        return ResponseEntity.noContent().build();
    }
}
