package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.product.variation.GetProductVariationResponse;
import com.mutatedstrings.ecommerceapi.model.ProductVariation;
import com.mutatedstrings.ecommerceapi.service.ProductVariationService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.PRODUCT_VARIATIONS)
public class ProductVariationsController {
    @Autowired
    private ProductVariationService productVariationService;

    @GetMapping
    public List<GetProductVariationResponse> getAll(@PathVariable String productId) {
        return ObjectMapperUtil.mapAll(productVariationService.getAll(productId), GetProductVariationResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetProductVariationResponse> get(@PathVariable String productId, @PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(productVariationService.get(productId, id), GetProductVariationResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetProductVariationResponse> create(@RequestBody GetProductVariationResponse getProductVariationResponse,
                                                              @PathVariable String productId) {
        getProductVariationResponse.setProductId(productId);
        ProductVariation productVariation = productVariationService.create(ObjectMapperUtil.map(getProductVariationResponse, ProductVariation.class), productId);
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(productVariation, GetProductVariationResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetProductVariationResponse> update(@RequestBody GetProductVariationResponse getProductVariationResponse,
                                                   @PathVariable String productId, @PathVariable Integer id) {
        getProductVariationResponse.setVariationId(id);
        getProductVariationResponse.setProductId(productId);
        ProductVariation productVariation = productVariationService.update(ObjectMapperUtil.map(getProductVariationResponse, ProductVariation.class), productId);
        return ResponseEntity.ok(ObjectMapperUtil.map(productVariation, GetProductVariationResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String productId, @PathVariable Integer id) {
        productVariationService.delete(productId, id);
        return ResponseEntity.noContent().build();
    }
}
