package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.product.GetProductResponse;
import com.mutatedstrings.ecommerceapi.dto.product.PostProductRequest;
import com.mutatedstrings.ecommerceapi.model.Product;
import com.mutatedstrings.ecommerceapi.service.ProductService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.PRODUCTS)
public class ProductsController {
    @Autowired
    private ProductService productService;

    /**
     * Get all active products
     * @param categoryCode
     * @param type
     * @return Product list with status code 200 if successful
     */
    @GetMapping
    public List<GetProductResponse> getAll(@RequestParam(value = "categoryCode", required = false) String categoryCode,
                                           @RequestParam(value = "type", required = false) String type) {
        return ObjectMapperUtil.mapAll(productService.getAll(categoryCode, type), GetProductResponse.class);
    }

    /**
     * Get details of a single product item
     * @param id    Product id
     * @return      GetProductResponse dto object. Status code 200
     */
    @GetMapping("/{id}")
    public ResponseEntity<GetProductResponse> get(@PathVariable String id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(productService.get(id), GetProductResponse.class));
    }

    /**
     * Create a new product item
     * @param postProductRequest    Product object to create
     * @return                      The created object. Status code 201
     */
    @PostMapping
    public ResponseEntity<GetProductResponse> create(@RequestBody PostProductRequest postProductRequest) {
        Product product = productService.create(ObjectMapperUtil.map(postProductRequest, Product.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(product, GetProductResponse.class));
    }

    /**
     * Updates an existing product item
     * @param postProductRequest    The details to update
     * @param id                    Product id
     * @return                      The updated object. Status code 200
     */
    @PutMapping("/{id}")
    public ResponseEntity<GetProductResponse> update(@RequestBody PostProductRequest postProductRequest,
                                                     @PathVariable String id) {
        postProductRequest.setProductId(id);
        Product product = productService.update(ObjectMapperUtil.map(postProductRequest, Product.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(product, GetProductResponse.class));
    }

    /**
     * Deletes a product item
     * @param id    Product id
     * @return      Status code 204
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }
}