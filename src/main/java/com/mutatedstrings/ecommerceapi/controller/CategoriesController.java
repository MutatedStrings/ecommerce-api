package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.category.GetCategoryResponse;
import com.mutatedstrings.ecommerceapi.dto.category.PostCategoryRequest;
import com.mutatedstrings.ecommerceapi.model.Category;
import com.mutatedstrings.ecommerceapi.service.CategoryService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.CATEGORIES)
public class CategoriesController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public List<GetCategoryResponse> getAll() {
        return ObjectMapperUtil.mapAll(categoryService.getAll(), GetCategoryResponse.class);
    }

    @GetMapping("/{categoryCode}")
    public ResponseEntity<GetCategoryResponse> get(@PathVariable String categoryCode) {
        return ResponseEntity.ok((ObjectMapperUtil.map(categoryService.get(categoryCode), GetCategoryResponse.class)));
    }

    @PostMapping
    public ResponseEntity<GetCategoryResponse> create(@RequestBody PostCategoryRequest postCategoryRequest) {
        Category newCategory = categoryService.create(ObjectMapperUtil.map(postCategoryRequest, Category.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(newCategory, GetCategoryResponse.class));
    }

    @PutMapping("/{categoryCode}")
    public ResponseEntity<GetCategoryResponse> update(@RequestBody PostCategoryRequest postCategoryRequest, @PathVariable String categoryCode) {
        Category updatedCategory = categoryService.update(ObjectMapperUtil.map(postCategoryRequest, Category.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(updatedCategory, GetCategoryResponse.class));
    }

    @DeleteMapping("/{categoryCode}")
    public ResponseEntity delete(@PathVariable String categoryCode) {
        categoryService.delete(categoryCode);
        return ResponseEntity.noContent().build();
    }
}
