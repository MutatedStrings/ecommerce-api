package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.sale.GetSaleDetailsResponse;
import com.mutatedstrings.ecommerceapi.dto.sale.GetSaleResponse;
import com.mutatedstrings.ecommerceapi.dto.sale.PostSaleRequest;
import com.mutatedstrings.ecommerceapi.model.Sale;
import com.mutatedstrings.ecommerceapi.service.SaleService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.ORDERS)
public class SalesController {
    @Autowired
    private SaleService saleService;

    @GetMapping
    public List<GetSaleResponse> getAll(@RequestParam(value = "userId", required = false) String userId) {
        return ObjectMapperUtil.mapAll(saleService.getAll(userId), GetSaleResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetSaleDetailsResponse> get(@PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(saleService.get(id), GetSaleDetailsResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetSaleDetailsResponse> create(@RequestBody PostSaleRequest postSaleRequest) {
        Sale sale = saleService.create(ObjectMapperUtil.map(postSaleRequest, Sale.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(sale, GetSaleDetailsResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetSaleDetailsResponse> update(@RequestBody PostSaleRequest postSaleRequest, @RequestParam Integer id) {
        postSaleRequest.setSaleId(id);
        Sale sale = saleService.update(ObjectMapperUtil.map(postSaleRequest, Sale.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(sale, GetSaleDetailsResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        saleService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
