package com.mutatedstrings.ecommerceapi.controller;

import com.mutatedstrings.ecommerceapi.dto.sale.saleItems.GetSaleItemResponse;
import com.mutatedstrings.ecommerceapi.dto.sale.saleItems.PostSaleItemRequest;
import com.mutatedstrings.ecommerceapi.model.SaleItem;
import com.mutatedstrings.ecommerceapi.service.SaleItemService;
import com.mutatedstrings.ecommerceapi.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiName.ORDER_ITEMS)
public class SaleItemsController {
    @Autowired
    private SaleItemService saleItemService;

    @GetMapping
    public List<GetSaleItemResponse> getAll() {
        return ObjectMapperUtil.mapAll(saleItemService.getAll(), GetSaleItemResponse.class);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetSaleItemResponse> get(@PathVariable Integer id) {
        return ResponseEntity.ok(ObjectMapperUtil.map(saleItemService.get(id), GetSaleItemResponse.class));
    }

    @PostMapping
    public ResponseEntity<GetSaleItemResponse> create(@RequestBody PostSaleItemRequest postSaleItemRequest) {
        SaleItem saleItem = saleItemService.create(ObjectMapperUtil.map(postSaleItemRequest, SaleItem.class));
        return ResponseEntity.status(HttpStatus.CREATED).body(ObjectMapperUtil.map(saleItem, GetSaleItemResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetSaleItemResponse> update(@RequestBody PostSaleItemRequest postSaleItemRequest, @PathVariable Integer id) {
        postSaleItemRequest.setSaleItemId(id);
        SaleItem saleItem = saleItemService.update(ObjectMapperUtil.map(postSaleItemRequest, SaleItem.class));
        return ResponseEntity.ok(ObjectMapperUtil.map(saleItem, GetSaleItemResponse.class));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        saleItemService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
