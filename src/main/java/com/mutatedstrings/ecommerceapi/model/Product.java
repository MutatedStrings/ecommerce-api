package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Product {
    private String productId;
    private String name;
    private BigDecimal price;
    private BigDecimal discountPercent;
    private boolean status;
    private String imageUrl;
    @JsonBackReference
    private Category category;
    @JsonManagedReference
    private Collection<ProductVariation> productVariations;

    @Id
    @GeneratedValue(generator = "product_generator")
    @GenericGenerator(name = "product_generator", parameters = @org.hibernate.annotations.Parameter(name = "prefix", value = "P"),
            strategy = "com.mutatedstrings.ecommerceapi.util.IdGenerator")
    @Column(name = "product_id", nullable = false, length = 5)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "discount_percent", nullable = false, precision = 2)
    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    @Basic
    @Column(name = "status", nullable = false, insertable = false)
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "image_url", nullable = false)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return status == product.status &&
                Objects.equals(productId, product.productId) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(discountPercent, product.discountPercent) &&
                Objects.equals(imageUrl, product.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, name, price, discountPercent, status, imageUrl);
    }

    @ManyToOne
    @JoinColumn(name = "category_code", referencedColumnName = "category_code", nullable = false)
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    public Collection<ProductVariation> getProductVariations() {
        return productVariations;
    }

    public void setProductVariations(Collection<ProductVariation> productVariations) {
        this.productVariations = productVariations;
    }
}
