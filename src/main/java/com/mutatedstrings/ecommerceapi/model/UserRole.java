package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user_role", schema = "eea_ecomm", catalog = "")
public class UserRole {
    private String roleId;
    private String roleTitle;
    @JsonManagedReference
    private Collection<User> users;

    @Id
    @Column(name = "role_id", nullable = false, length = 3)
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role_title", nullable = false, length = 15)
    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRole userRole = (UserRole) o;
        return Objects.equals(roleId, userRole.roleId) &&
                Objects.equals(roleTitle, userRole.roleTitle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleTitle);
    }

    @OneToMany(mappedBy = "userRole")
    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }
}
