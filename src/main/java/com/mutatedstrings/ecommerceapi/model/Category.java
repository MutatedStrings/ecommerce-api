package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Category {
    private String categoryCode;
    private String categoryName;
    private String imageUrl;
    private String type;
    private boolean status;
    @JsonManagedReference
    private Collection<Product> products;

    @Id
    @Column(name = "category_code", nullable = false, length = 5)
    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    @Basic
    @Column(name = "category_name", nullable = false, length = 25)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Basic
    @Column(name = "image_url", nullable = false, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 20)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "status", nullable = false, insertable = false)
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return status == category.status &&
                Objects.equals(categoryCode, category.categoryCode) &&
                Objects.equals(categoryName, category.categoryName) &&
                Objects.equals(imageUrl, category.imageUrl) &&
                Objects.equals(type, category.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryCode, categoryName, imageUrl, type, status);
    }

    @OneToMany(mappedBy = "category")
    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
