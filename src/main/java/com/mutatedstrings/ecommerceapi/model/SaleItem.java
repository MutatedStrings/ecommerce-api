package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "sale_item", schema = "eea_ecomm", catalog = "")
public class SaleItem {
    private int saleItemId;
    private int qty;
    private BigDecimal pricePerUnit;
    private boolean status;
    @JsonBackReference
    private Sale sale;
    @JsonBackReference
    private ProductVariation productVariation;

    @Id
    @GeneratedValue
    @Column(name = "sale_item_id", nullable = false)
    public int getSaleItemId() {
        return saleItemId;
    }

    public void setSaleItemId(int saleItemId) {
        this.saleItemId = saleItemId;
    }

    @Basic
    @Column(name = "qty", nullable = false)
    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Basic
    @Column(name = "price_per_unit", nullable = false, precision = 2)
    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @Basic
    @Column(name = "status", nullable = false, insertable = false)
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleItem saleItem = (SaleItem) o;
        return saleItemId == saleItem.saleItemId &&
                qty == saleItem.qty &&
                status == saleItem.status &&
                Objects.equals(pricePerUnit, saleItem.pricePerUnit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleItemId, qty, pricePerUnit, status);
    }

    @ManyToOne
    @JoinColumn(name = "sale_id", referencedColumnName = "sale_id", nullable = false)
    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    @ManyToOne
    @JoinColumn(name = "variation_id", referencedColumnName = "variation_id", nullable = false)
    public ProductVariation getProductVariation() {
        return productVariation;
    }

    public void setProductVariation(ProductVariation productVariation) {
        this.productVariation = productVariation;
    }
}
