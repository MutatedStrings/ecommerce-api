package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "credit_card", schema = "eea_ecomm", catalog = "")
public class CreditCard {
    private int cardId;
    private String cardNumber;
    private String cardType;
    private int expirationMonth;
    private int expirationYear;
    private String holderName;
    @JsonBackReference
    private User user;

    @Id
    @GeneratedValue
    @Column(name = "card_id", nullable = false)
    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    @Basic
    @Column(name = "card_number", nullable = false, length = 16)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "card_type", nullable = false, length = 50)
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Basic
    @Column(name = "expiration_month", nullable = false)
    public int getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(int expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    @Basic
    @Column(name = "expiration_year", nullable = false)
    public int getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(int expirationYear) {
        this.expirationYear = expirationYear;
    }

    @Basic
    @Column(name = "holder_name", nullable = false, length = 25)
    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditCard that = (CreditCard) o;
        return cardId == that.cardId &&
                expirationMonth == that.expirationMonth &&
                expirationYear == that.expirationYear &&
                Objects.equals(cardNumber, that.cardNumber) &&
                Objects.equals(cardType, that.cardType) &&
                Objects.equals(holderName, that.holderName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId, cardNumber, cardType, expirationMonth, expirationYear, holderName);
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
