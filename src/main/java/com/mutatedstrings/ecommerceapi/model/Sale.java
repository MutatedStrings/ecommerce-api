package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Sale {
    private int saleId;
    private Date orderDate;
    private BigDecimal totalAmount;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String addressLine1;
    private String addressLine2;
    private String street;
    private String city;
    private String country;
    private boolean status;
    @JsonBackReference
    private User user;
    @JsonManagedReference
    private Collection<SaleItem> saleItems;

    @Id
    @GeneratedValue
    @Column(name = "sale_id", nullable = false)
    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    @Basic
    @Column(name = "order_date", nullable = false)
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "total_amount", nullable = false, precision = 2)
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 20)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "card_number", nullable = false, length = 16)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "address_line_1", nullable = false, length = 100)
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Basic
    @Column(name = "address_line_2", nullable = false, length = 100)
    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Basic
    @Column(name = "street", nullable = false, length = 50)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "city", nullable = false, length = 30)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "country", nullable = false, length = 30)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "status", nullable = false, insertable = false)
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sale sale = (Sale) o;
        return saleId == sale.saleId &&
                status == sale.status &&
                Objects.equals(orderDate, sale.orderDate) &&
                Objects.equals(totalAmount, sale.totalAmount) &&
                Objects.equals(firstName, sale.firstName) &&
                Objects.equals(lastName, sale.lastName) &&
                Objects.equals(cardNumber, sale.cardNumber) &&
                Objects.equals(addressLine1, sale.addressLine1) &&
                Objects.equals(addressLine2, sale.addressLine2) &&
                Objects.equals(street, sale.street) &&
                Objects.equals(city, sale.city) &&
                Objects.equals(country, sale.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(saleId, orderDate, totalAmount, firstName, lastName, cardNumber, addressLine1, addressLine2, street, city, country, status);
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(mappedBy = "sale", cascade = CascadeType.ALL)
    public Collection<SaleItem> getSaleItems() {
        return saleItems;
    }

    public void setSaleItems(Collection<SaleItem> saleItems) {
        this.saleItems = saleItems;
    }
}
