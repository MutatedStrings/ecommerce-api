package com.mutatedstrings.ecommerceapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cart_item", schema = "eea_ecomm", catalog = "")
public class CartItem {
    private int cartItemId;
    private int quantity;
    @JsonBackReference
    private ProductVariation productVariation;
    @JsonBackReference
    private User user;

    @Id
    @GeneratedValue
    @Column(name = "cart_item_id", nullable = false)
    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    @Basic
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItem cartItem = (CartItem) o;
        return cartItemId == cartItem.cartItemId &&
                quantity == cartItem.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartItemId, quantity);
    }

    @ManyToOne
    @JoinColumn(name = "variation_id", referencedColumnName = "variation_id", nullable = false)
    public ProductVariation getProductVariation() {
        return productVariation;
    }

    public void setProductVariation(ProductVariation productVariationByVariationId) {
        this.productVariation = productVariationByVariationId;
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
