package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Integer> {
    List<CreditCard> findAllByUserUserId(String userId);
    Optional<CreditCard> findByCardIdAndUserUserId(Integer id, String userId);
}
