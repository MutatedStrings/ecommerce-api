package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.ProductVariation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductVariationRepository extends JpaRepository<ProductVariation, Integer> {
    List<ProductVariation> findAllByProductProductId(String productId);
    Optional<ProductVariation> findByVariationIdAndProductProductId(Integer id, String productId);
}
