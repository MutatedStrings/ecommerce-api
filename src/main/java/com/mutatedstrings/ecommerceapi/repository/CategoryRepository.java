package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
}
