package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.BillingAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BillingAddressRepository extends JpaRepository<BillingAddress, Integer> {
    List<BillingAddress> findAllByUserUserId(String id);
    Optional<BillingAddress> findByAddressIdAndUserUserId(Integer id, String userId);
}
