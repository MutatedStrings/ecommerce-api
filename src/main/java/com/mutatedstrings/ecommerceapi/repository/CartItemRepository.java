package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
    List<CartItem> findAllByUserUserId(String userId);
    Optional<CartItem> findByCartItemIdAndUserUserId(Integer id, String userId);
    Optional<CartItem> findByProductVariationVariationIdAndUserUserId(Integer variationId, String userId);
    void deleteAllByUserUserId(String userId);
}
