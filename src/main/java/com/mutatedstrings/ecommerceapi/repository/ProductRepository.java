package com.mutatedstrings.ecommerceapi.repository;

import com.mutatedstrings.ecommerceapi.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByStatusTrue();
    List<Product> findAllByStatusTrueAndCategoryCategoryCode(String categoryCode);
    List<Product> findAllByStatusTrueAndCategoryType(String type);
    List<Product> findAllByStatusTrueAndCategoryCategoryCodeAndCategoryType(String categoryCode, String type);
    Optional<Product> findByProductIdAndStatusTrue(String id);
}
