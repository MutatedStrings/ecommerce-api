package com.mutatedstrings.ecommerceapi.dto.user;

import com.mutatedstrings.ecommerceapi.dto.userRole.GetUserRoleResponse;

public class GetUserResponse {
    private String userId;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private GetUserRoleResponse userRole;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public GetUserRoleResponse getUserRole() {
        return userRole;
    }

    public void setUserRole(GetUserRoleResponse userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "GetUserResponse{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", userRole=" + userRole +
                '}';
    }
}
