package com.mutatedstrings.ecommerceapi.dto.user.cartItem;

import com.mutatedstrings.ecommerceapi.dto.product.variation.GetDetailedProductVariationResponse;
import com.mutatedstrings.ecommerceapi.dto.product.variation.GetProductVariationResponse;

public class GetCartItemResponse {
    private int cartItemId;
    private int quantity;
    private GetDetailedProductVariationResponse productVariation;
    private String userId;

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public GetDetailedProductVariationResponse getProductVariation() {
        return productVariation;
    }

    public void setProductVariation(GetDetailedProductVariationResponse productVariation) {
        this.productVariation = productVariation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GetCartItemResponse{" +
                "cartItemId=" + cartItemId +
                ", quantity=" + quantity +
                ", productVariation=" + productVariation +
                ", userId='" + userId + '\'' +
                '}';
    }
}
