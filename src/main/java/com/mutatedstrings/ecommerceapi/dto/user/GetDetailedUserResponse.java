package com.mutatedstrings.ecommerceapi.dto.user;

import com.mutatedstrings.ecommerceapi.dto.user.billingAddress.GetBillingAddressResponse;
import com.mutatedstrings.ecommerceapi.dto.user.creditCard.GetCreditCardResponse;
import com.mutatedstrings.ecommerceapi.dto.userRole.GetUserRoleResponse;

import java.util.Collection;

public class GetDetailedUserResponse {
    private String userId;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private boolean status;
    private Collection<GetBillingAddressResponse> billingAddresses;
    private Collection<GetCreditCardResponse> creditCards;
    private GetUserRoleResponse userRole;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Collection<GetBillingAddressResponse> getBillingAddresses() {
        return billingAddresses;
    }

    public void setBillingAddresses(Collection<GetBillingAddressResponse> billingAddresses) {
        this.billingAddresses = billingAddresses;
    }

    public Collection<GetCreditCardResponse> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Collection<GetCreditCardResponse> creditCards) {
        this.creditCards = creditCards;
    }

    public GetUserRoleResponse getUserRole() {
        return userRole;
    }

    public void setUserRole(GetUserRoleResponse userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "GetDetailedUserResponse{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", status=" + status +
                ", billingAddresses=" + billingAddresses +
                ", creditCards=" + creditCards +
                ", userRole=" + userRole +
                '}';
    }
}
