package com.mutatedstrings.ecommerceapi.dto.user.cartItem;

public class PostCartItemRequest {
    private int cartItemId;
    private int quantity;
    private int productVariationId;
    private String userId;

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getProductVariationId() {
        return productVariationId;
    }

    public void setProductVariationId(int productVariationId) {
        this.productVariationId = productVariationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PostCartItemRequest{" +
                "cartItemId=" + cartItemId +
                ", quantity=" + quantity +
                ", productVariationId='" + productVariationId + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
