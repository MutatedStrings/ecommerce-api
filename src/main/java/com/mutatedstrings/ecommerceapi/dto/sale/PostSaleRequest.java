package com.mutatedstrings.ecommerceapi.dto.sale;

import com.mutatedstrings.ecommerceapi.dto.sale.saleItems.GetSaleItemResponse;
import com.mutatedstrings.ecommerceapi.dto.sale.saleItems.PostSaleItemRequest;
import com.mutatedstrings.ecommerceapi.dto.user.GetUserResponse;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

public class PostSaleRequest {
    private int saleId;
    private Date orderDate;
    private BigDecimal totalAmount;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private String addressLine1;
    private String addressLine2;
    private String street;
    private String city;
    private String country;
    private String userId;
    private Collection<PostSaleItemRequest> saleItems;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Collection<PostSaleItemRequest> getSaleItems() {
        return saleItems;
    }

    public void setSaleItems(Collection<PostSaleItemRequest> saleItems) {
        this.saleItems = saleItems;
    }

    @Override
    public String toString() {
        return "PostSaleRequest{" +
                "saleId=" + saleId +
                ", orderDate=" + orderDate +
                ", totalAmount=" + totalAmount +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", userId='" + userId + '\'' +
                ", saleItems=" + saleItems +
                '}';
    }
}
