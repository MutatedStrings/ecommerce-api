package com.mutatedstrings.ecommerceapi.dto.sale.saleItems;

import com.mutatedstrings.ecommerceapi.dto.product.variation.GetDetailedProductVariationResponse;

import java.math.BigDecimal;

public class PostSaleItemRequest {
    private int saleItemId;
    private int qty;
    private BigDecimal pricePerUnit;
    private int saleId;
    private String productVariationId;

    public int getSaleItemId() {
        return saleItemId;
    }

    public void setSaleItemId(int saleItemId) {
        this.saleItemId = saleItemId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getProductVariationId() {
        return productVariationId;
    }

    public void setProductVariationId(String productVariationId) {
        this.productVariationId = productVariationId;
    }

    @Override
    public String toString() {
        return "PostSaleItemRequest{" +
                "saleItemId=" + saleItemId +
                ", qty=" + qty +
                ", pricePerUnit=" + pricePerUnit +
                ", saleId=" + saleId +
                ", productVariationId='" + productVariationId + '\'' +
                '}';
    }
}
