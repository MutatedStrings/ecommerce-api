package com.mutatedstrings.ecommerceapi.dto.sale;

import com.mutatedstrings.ecommerceapi.dto.user.GetUserResponse;

import java.math.BigDecimal;
import java.util.Date;

public class GetSaleResponse {
    private int saleId;
    private Date orderDate;
    private BigDecimal totalAmount;
    private GetUserResponse user;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public GetUserResponse getUser() {
        return user;
    }

    public void setUser(GetUserResponse user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GetSaleResponse{" +
                "saleId=" + saleId +
                ", orderDate=" + orderDate +
                ", totalAmount=" + totalAmount +
                ", user=" + user +
                '}';
    }
}
