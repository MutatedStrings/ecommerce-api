package com.mutatedstrings.ecommerceapi.dto.product;

import com.mutatedstrings.ecommerceapi.dto.category.GetCategoryResponse;
import com.mutatedstrings.ecommerceapi.dto.product.variation.GetProductVariationResponse;

import java.math.BigDecimal;
import java.util.Collection;

public class GetProductResponse {
    private String productId;
    private String name;
    private BigDecimal price;
    private BigDecimal discountPercent;
    private String imageUrl;
    private GetCategoryResponse category;
    private Collection<GetProductVariationResponse> productVariations;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public GetCategoryResponse getCategory() {
        return category;
    }

    public void setCategory(GetCategoryResponse category) {
        this.category = category;
    }

    public Collection<GetProductVariationResponse> getProductVariations() {
        return productVariations;
    }

    public void setProductVariations(Collection<GetProductVariationResponse> productVariations) {
        this.productVariations = productVariations;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "GetProductResponse{" +
                "productId='" + productId + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", discountPercent=" + discountPercent +
                ", imageUrl='" + imageUrl + '\'' +
                ", category=" + category +
                ", productVariations=" + productVariations +
                '}';
    }
}
