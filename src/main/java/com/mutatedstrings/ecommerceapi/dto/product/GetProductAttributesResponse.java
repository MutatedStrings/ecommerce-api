package com.mutatedstrings.ecommerceapi.dto.product;

import java.math.BigDecimal;

public class GetProductAttributesResponse {
    private String productId;
    private String name;
    private BigDecimal price;
    private BigDecimal discountPercent;
    private String imageUrl;
    private String categoryCode;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    @Override
    public String toString() {
        return "GetProductAttributesResponse{" +
                "productId='" + productId + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", discountPercent=" + discountPercent +
                ", imageUrl='" + imageUrl + '\'' +
                ", categoryCode='" + categoryCode + '\'' +
                '}';
    }
}
