package com.mutatedstrings.ecommerceapi.dto.product.variation;

import com.mutatedstrings.ecommerceapi.dto.product.GetProductAttributesResponse;

public class GetDetailedProductVariationResponse {
    private int variationId;
    private String imageUrl;
    private String color;
    private String size;
    private int quantity;
    private GetProductAttributesResponse product;

    public int getVariationId() {
        return variationId;
    }

    public void setVariationId(int variationId) {
        this.variationId = variationId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public GetProductAttributesResponse getProduct() {
        return product;
    }

    public void setProduct(GetProductAttributesResponse product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "GetDetailedProductVariationResponse{" +
                "variationId='" + variationId + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", color='" + color + '\'' +
                ", size='" + size + '\'' +
                ", quantity=" + quantity +
                ", product=" + product +
                '}';
    }
}
