package com.mutatedstrings.ecommerceapi.dto.userRole;

public class GetUserRoleResponse {
    private String roleId;
    private String roleTitle;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    @Override
    public String toString() {
        return "GetUserRoleResponse{" +
                "roleId='" + roleId + '\'' +
                ", roleTitle='" + roleTitle + '\'' +
                '}';
    }
}
