package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.UserRole;
import com.mutatedstrings.ecommerceapi.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    public Page<UserRole> getAll(Pageable pageable) {
        return userRoleRepository.findAll(pageable);
    }

    public UserRole get(String id) {
        return userRoleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
    }

    public UserRole create(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

    public UserRole update(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

    public boolean delete(String id) {
        userRoleRepository.delete(get(id));
        return true;
    }
}
