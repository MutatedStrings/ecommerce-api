package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.User;
import com.mutatedstrings.ecommerceapi.model.UserRole;
import com.mutatedstrings.ecommerceapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User get(String id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElse(null);
    }

    public User create(User user) {
        UserRole userRole = new UserRole();
        userRole.setRoleId("Cus");
        user.setUserRole(userRole);
        user.setPassword(PASSWORD_ENCODER.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User update(User user) {
        isExist(user.getUserId());
        return userRepository.save((user));
    }

    public boolean delete(String id) {
        userRepository.delete(get(id));
        return true;
    }

    /**
     * Checks if the particular user exists. Throws a ResourceNotFoundException if user not found
     *
     * @param id
     */
    public void isExist(String id) {
        if (!userRepository.existsById(id)) {
            throw new ResourceNotFoundException("User resource " + id + " not found.");
        }
    }

    public User getUserByEmailAndPass(String email, String password) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Invalid credentials!"));
        if(!PASSWORD_ENCODER.matches(password, user.getPassword())) {
            throw new UsernameNotFoundException("Invalid credentials!");
        }

        return user;
    }
}
