package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.Product;
import com.mutatedstrings.ecommerceapi.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    /**
     * Gets a collection of active products
     * @param categoryCode
     * @param type
     * @return  List of all active Products
     */
    public List<Product> getAll(String categoryCode, String type) {
        if(categoryCode != null) {
            return productRepository.findAllByStatusTrueAndCategoryCategoryCode(categoryCode);
        } else if(type != null) {
            return productRepository.findAllByStatusTrueAndCategoryType(type);
        } else if(categoryCode != null && type != null) {
            return productRepository.findAllByStatusTrueAndCategoryCategoryCodeAndCategoryType(categoryCode, type);
        }
        else {
            return productRepository.findAllByStatusTrue();
        }
    }

    /**
     * Gets a single product
     * @param id    Product id
     * @return      Product. Throws a ResourceNotFoundException if not found
     */
    public Product get(String id) {
        return productRepository.findByProductIdAndStatusTrue(id).orElseThrow(() -> new ResourceNotFoundException("Product resource " + id + " not found."));
    }

    /**
     * Creates a new Product
     * @param product   Product object
     * @return          The created product object
     */
    public Product create(Product product) {
        product.getProductVariations().forEach(variation -> variation.setProduct(product));
        return productRepository.save(product);
    }

    /**
     * Updates an already existing product
     * @param product   Product object
     * @return          The updated product object
     */
    public Product update(Product product) {
        isExist(product.getProductId());
        return productRepository.save(product);
    }

    /**
     * Deletes a product item. Sets the product status to false
     * @param id    Product id
     * @return      True
     */
    public boolean delete(String id) {
        //TODO Set all product variation statuses to false
        Product product = get(id);
        product.setStatus(false);
        return true;
    }

    /**
     * Checks if a particular product exists. Throws a ResourceNotFoundException if not found
     * @param id    Product id
     */
    public void isExist(String id) {
        if(!productRepository.existsById(id)) {
            throw new ResourceNotFoundException("Product resource " + id + " not found.");
        }
    }
}
