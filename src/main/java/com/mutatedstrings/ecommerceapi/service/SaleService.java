package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.Sale;
import com.mutatedstrings.ecommerceapi.model.SaleItem;
import com.mutatedstrings.ecommerceapi.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

@Service
public class SaleService {
    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private CartItemService cartItemService;

    public List<Sale> getAll(String userId) {
        if(userId != null) {
            return saleRepository.findAllByUserUserId(userId);
        }
        return saleRepository.findAll();
    }

    public Sale get(Integer id) {
        return saleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
    }

    public Sale create(Sale sale) {
        if(sale.getOrderDate() == null) {
            sale.setOrderDate(new Date(new java.util.Date().getTime()));
        }
        sale.getSaleItems().forEach(item -> item.setSale(sale));
        Sale newSale = saleRepository.save(sale);
        cartItemService.deleteAllByUser(sale.getUser().getUserId());
        return newSale;
    }

    public Sale update(Sale sale) {
        isExist(sale.getSaleId());
        return saleRepository.save(sale);
    }

    public boolean delete(Integer id) {
        saleRepository.delete(get(id));
        return true;
    }

    /**
     * Checks if a particular sale exists. Throws a ResourceNotFoundException if not found
     * @param id    Sale id
     */
    public void isExist(Integer id) {
        if(!saleRepository.existsById(id)) {
            throw new ResourceNotFoundException("Sale resource " + id + " not found.");
        }
    }
}
