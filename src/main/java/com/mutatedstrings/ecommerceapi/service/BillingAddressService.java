package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.BillingAddress;
import com.mutatedstrings.ecommerceapi.repository.BillingAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillingAddressService {
    @Autowired
    private BillingAddressRepository billingAddressRepository;
    @Autowired
    private UserService userService;

    /**
     * Get all addresses for a particular user
     * @param id    User id
     * @return      List of BillingAddress for user. If user does not exist throw a ResourceNotFoundException
     */
    public List<BillingAddress> getAllByUserId(String id) {
        userService.isExist(id);
        return billingAddressRepository.findAllByUserUserId(id);
    }

    /**
     * Get details of a single billing address
     * @param userId    User id
     * @param id        Billing address id
     * @return          Billing address object. Throws a resource not found exception if a billing address with the id
     *                  cannot be found
     */
    public BillingAddress get(String userId, Integer id) {
        return billingAddressRepository.findByAddressIdAndUserUserId(id, userId).orElseThrow(() -> new ResourceNotFoundException());
    }

    /**
     * Add a new billing address
     * @param billingAddress    Billing address object
     * @param userId            User id
     * @return                  Returns the created BillingAddress object
     */
    public BillingAddress create(BillingAddress billingAddress, String userId) {
        userService.isExist(userId);
        return billingAddressRepository.save(billingAddress);
    }

    /**
     * Update details of an existing billing address
     * @param billingAddress    Billing address object
     * @param userId            User id
     * @return                  Returns the updated BillingAddress object
     */
    public BillingAddress update(BillingAddress billingAddress, String userId) {
        userService.isExist(userId);
        isExist(billingAddress.getAddressId());
        return billingAddressRepository.save(billingAddress);
    }

    /**
     * Delete billing address
     * @param userId    User id
     * @param id        Billing address id
     * @return          Returns true
     */
    public boolean delete(String userId, Integer id) {
        billingAddressRepository.delete(get(userId, id));
        return true;
    }

    /**
     * Checks if a particular billing address exists. Throws a ResourceNotFoundException if not found
     * @param id
     */
    public void isExist(Integer id) {
        if(!billingAddressRepository.existsById(id)) {
            throw new ResourceNotFoundException("Billing address resource " + id + " not found.");
        }
    }
}
