package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.CreditCard;
import com.mutatedstrings.ecommerceapi.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditCardService {
    @Autowired
    private CreditCardRepository creditCardRepository;
    @Autowired
    private UserService userService;

    public List<CreditCard> getAllByUserId(String userId) {
        return creditCardRepository.findAllByUserUserId(userId);
    }

    public CreditCard get(String userId, Integer id) {
        return creditCardRepository.findByCardIdAndUserUserId(id, userId).orElseThrow(() -> new ResourceNotFoundException());
    }

    public CreditCard create(CreditCard creditCard, String userId) {
        userService.isExist(userId);
        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard creditCard, String userId) {
        userService.isExist(userId);
        return creditCardRepository.save(creditCard);
    }

    public boolean delete(String userId, Integer id) {
        userService.isExist(userId);
        creditCardRepository.delete(get(userId, id));
        return true;
    }

    /**
     * Checks if a particular credit card exists. Throws a ResourceNotFoundException if not found
     * @param id
     */
    public void isExist(Integer id) {
        if(!creditCardRepository.existsById(id)) {
            throw new ResourceNotFoundException("Credit card resource " + id + " not found.");
        }
    }
}
