package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.CartItem;
import com.mutatedstrings.ecommerceapi.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CartItemService {
    @Autowired
    private CartItemRepository cartItemRepository;
    @Autowired
    private UserService userService;

    /**
     * Get all cart items for a particular user
     * @param userId    User id
     * @return          List of CartItem objects that belong to the user. If user not found ResourceNotFoundException
     *                  is thrown
     */
    public List<CartItem> getAllByUserId(String userId) {
        userService.isExist(userId);
        return cartItemRepository.findAllByUserUserId(userId);
    }

    /**
     * Get a single cart item
     * @param userId    User id
     * @param id        Cart item id
     * @return          Cart Item details. Throws a ResourceNotFoundException if not found
     */
    public CartItem get(String userId, Integer id) {
        return cartItemRepository.findByCartItemIdAndUserUserId(id, userId).orElseThrow(() -> new ResourceNotFoundException());
    }

    /**
     * Add a new cart item object. If cart item with given variation and user exists, then update
     * @param cartItem  CartItem object to create
     * @param userId    User id
     * @return          Returns the created CartItem object
     */
    public CartItem create(CartItem cartItem, String userId) {
        userService.isExist(userId);
        CartItem existingCartItem = cartItemRepository.findByProductVariationVariationIdAndUserUserId(
                cartItem.getProductVariation().getVariationId(), cartItem.getUser().getUserId()).orElse(null);
        if(existingCartItem != null) {
            cartItem.setCartItemId(existingCartItem.getCartItemId());
        }
        return cartItemRepository.save(cartItem);
    }

    /**
     * Update an existing cart item
     * @param cartItem  CartItem object to update
     * @param userId    User id
     * @return          Returns the updated CartItem object
     */
    public CartItem update(CartItem cartItem, String userId) {
        userService.isExist(userId);
        isExist(cartItem.getCartItemId());
        return cartItemRepository.save(cartItem);
    }

    /**
     * Deletes a cart item from the cart
     * @param userId    User id
     * @param id        Cart item id
     * @return  true
     */
    public boolean delete(String userId, Integer id) {
        cartItemRepository.delete(get(userId, id));
        return true;
    }

    /**
     * Deletes all cart items belonging a particular user
     * @param userId    User id
     * @return  true
     */
    @Transactional
    public boolean deleteAllByUser(String userId) {
        userService.isExist(userId);
        cartItemRepository.deleteAllByUserUserId(userId);
        return true;
    }

    /**
     * Checks if a particular cart item exists. Throws a ResourceNotFoundException if not found
     * @param id
     */
    public void isExist(Integer id) {
        if(!cartItemRepository.existsById(id)) {
            throw new ResourceNotFoundException("Cart item resource " + id + " not found.");
        }
    }
}
