package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.SaleItem;
import com.mutatedstrings.ecommerceapi.repository.SaleItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleItemService {
    @Autowired
    private SaleItemRepository saleItemRepository;

    public List<SaleItem> getAll() {
        return saleItemRepository.findAllByStatusTrue();
    }

    public SaleItem get(Integer id) {
        return saleItemRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException());
    }

    public SaleItem create(SaleItem saleItem) {
        return saleItemRepository.save(saleItem);
    }

    public SaleItem update(SaleItem saleItem) {
        isExist(saleItem.getSaleItemId());
        return saleItemRepository.save(saleItem);
    }

    public boolean delete(Integer id) {
        saleItemRepository.delete(get(id));
        return true;
    }

    /**
     * Checks if a particular sale item exists. Throws a ResourceNotFoundException if not found
     * @param id    Sale item id
     */
    public void isExist(Integer id) {
        if(!saleItemRepository.existsById(id)) {
            throw new ResourceNotFoundException("Sale item resource " + id + " not found.");
        }
    }
}
