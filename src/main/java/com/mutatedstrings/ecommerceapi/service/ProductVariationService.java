package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.ProductVariation;
import com.mutatedstrings.ecommerceapi.repository.ProductVariationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductVariationService {
    @Autowired
    private ProductVariationRepository productVariationRepository;
    @Autowired
    private ProductService productService;

    public List<ProductVariation> getAll(String id) {
        productService.isExist(id);
        return productVariationRepository.findAllByProductProductId(id);
    }

    public ProductVariation get(String productId, Integer id) {
        return productVariationRepository.findByVariationIdAndProductProductId(id, productId).orElseThrow(() -> new ResourceNotFoundException());
    }

    public ProductVariation create(ProductVariation productVariation, String productId) {
        productService.isExist(productId);
        return productVariationRepository.save(productVariation);
    }

    public ProductVariation update(ProductVariation productVariation, String productId) {
        productService.isExist(productId);
        isExist(productVariation.getVariationId());
        return productVariationRepository.save(productVariation);
    }

    public boolean delete(String productId, Integer id) {
        ProductVariation productVariation = get(productId, id);
        productVariation.setStatus(false);
        productVariationRepository.save(productVariation);
        return true;
    }

    /**
     * Checks if a particular product variation exists. Throws a ResourceNotFoundException if not found
     * @param id    Product variation id
     */
    public void isExist(Integer id) {
        if(!productVariationRepository.existsById(id)) {
            throw new ResourceNotFoundException("Product variation resource " + id + " not found.");
        }
    }
}
