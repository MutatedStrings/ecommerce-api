package com.mutatedstrings.ecommerceapi.service;

import com.mutatedstrings.ecommerceapi.exception.ResourceNotFoundException;
import com.mutatedstrings.ecommerceapi.model.Category;
import com.mutatedstrings.ecommerceapi.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    public Category get(String categoryCode) {
        return categoryRepository.findById(categoryCode).orElseThrow(() -> new ResourceNotFoundException());
    }

    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    public Category update(Category category) {
        return categoryRepository.save(category);
    }

    public boolean delete(String categoryCode) {
        categoryRepository.delete(get(categoryCode));
        return true;
    }
}
