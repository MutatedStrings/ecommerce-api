package com.mutatedstrings.ecommerceapi.auth;

import com.mutatedstrings.ecommerceapi.model.User;
import com.mutatedstrings.ecommerceapi.service.UserRoleService;
import com.mutatedstrings.ecommerceapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CustomAuthProvider implements AuthenticationProvider {
    private UserService userService;
    private UserRoleService userRoleService;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public CustomAuthProvider(UserService userService, UserRoleService userRoleService) {
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user = userService.getUserByEmail(email);
//        if (user != null && passwordEncoder.matches(password, user.getPassword()))
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
