package com.mutatedstrings.ecommerceapi.util;

import com.mutatedstrings.ecommerceapi.dto.sale.saleItems.GetSaleItemResponse;
import com.mutatedstrings.ecommerceapi.model.Product;
import com.mutatedstrings.ecommerceapi.model.SaleItem;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ObjectMapperUtil {

    private static ModelMapper modelMapper = new ModelMapper();
    private static int numMaps = -1;

    static {
        modelMapper = new ModelMapper();
    }

    /**
     * Create private constructor so cannot be instantiated by other classes
     */
    private ObjectMapperUtil() {
    }

    /**
     * Maps an object of type T to an object of type D
     *
     * @param <D>      type of result object.
     * @param <T>      type of source object to map from.
     * @param entity   entity that needs to be mapped.
     * @param outClass class of result object.
     * @return new mapped object of type D.
     */
    public static <D, T> D map(final T entity, Class<D> outClass) {
        addMappings();
        return modelMapper.map(entity, outClass);
    }

    /**
     * Maps List of type T to a List of type D
     *
     * @param entityList list of entities that needs to be mapped
     * @param outClass   class of result list element
     * @param <D>        type of objects in result list
     * @param <T>        type of entity in entityList
     * @return list of mapped object of type D.
     */
    public static <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass) {
        addMappings();
        return entityList.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }

    /**
     * Maps {@code source} to {@code destination}.
     *
     * @param source      object to map from
     * @param destination object to map to
     */
    public static <S, D> D map(final S source, D destination) {
        addMappings();
        modelMapper.map(source, destination);
        return destination;
    }

    /**
     * Maps Page of type S to a Page of type D
     *
     * @param entityPage    page of entities that needs to be mapped
     * @param outClass      the class the result page is to be mapped to
     * @param <S>           type of the objects in entityPage
     * @param <D>           type of the objects in the results page
     * @return page of mapped objects of type D
     */
    public static <S, D> Page<D> map(final Page<S> entityPage, Class<D> outClass) {
        addMappings();
        return entityPage
                .map(entity -> map(entity, outClass));
    }

    private static void addMappings() {
        if (numMaps > -1) {
            return;
        }
        PropertyMap<SaleItem, GetSaleItemResponse> getProductMap = new PropertyMap<SaleItem, GetSaleItemResponse>() {
            protected void configure() {
                map().setSaleItemId(source.getSaleItemId());
                map().setSaleId(source.getSale().getSaleId());
            }
        };
        modelMapper.addMappings(getProductMap);

        numMaps = modelMapper.getTypeMaps().size();
    }
}
